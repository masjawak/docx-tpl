<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class reportController extends Controller
{
    private $local_template_file;
    private $storageDisk;
    private $format_filename;

    public function __construct()
    {
        //name of disk for storage
        $this->storageDisk          = 'report';
        // Format file name downloaded
        $this->format_filename  = 'report_'.date('d-m-Y H i s').'.pdf';

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_json = $this->_data_json();
        $data = $this->parsing_data($data_json);

        dd($data);

    }

    public function show($param)
    {
        $data_json = $this->_data_json();
        if ($param == 'showroom') {
           // REPORT showroom
            return $this->generate_report($data_json, 'template-showroom.docx');
        }else{
           // REPORT Handover
            $this->generate_report($data_json, 'template-handover.docx');
        }

    }

    public function generate_report($data_json=array(), $file_name_template='')
    {
       
        $data = $this->parsing_data($data_json);

        //call class
        $docx_creation = new \WrkLst\DocxMustache\DocxMustache($data, $file_name_template);
        //optionally change some setting before the class gets executed
        $docx_creation->storageDisk = $this->storageDisk;
        //execute class
        $docx_creation->execute();
        //generated pdf file
        $docx_creation->SaveAsPdf();
        //rename file pdf
        $rename = $this->_rename($docx_creation->SaveAsPdf(), $file_name_template);
        // Download file pdf
        return response()->download($rename);
    }

    public function parsing_data($value=array())
    {
        $data  = json_decode($value, TRUE);

        $arr = array(
            'grade_total'                         => $data['grade']['total'],
            'grade_score'                         => $data['grade']['score'],
            'title'                               => $data['title'],
            'appraise_photos'                     => $data['appraise_photos'],
            'appraise_frame'                      => $data['appraise_frame'],
            'appraise_engine'                     => $data['appraise_engine'],
            'appraise_interior'                   => $data['appraise_interior'],
            'appraise_exterior'                   => $data['appraise_exterior'],
            'appraise_info_id_car_appraise'       => $data['appraise_info']['id_car_appraise'],
            'appraise_info_id_tradein'            => $data['appraise_info']['id_tradein'],
            'appraise_info_defect_exterior'       => $data['appraise_info']['defect_exterior'],
            'appraise_info_defect_interior'       => $data['appraise_info']['defect_interior'],
            'appraise_info_defect_engine'         => $data['appraise_info']['defect_engine'],
            'appraise_info_defect_frame'          => $data['appraise_info']['defect_frame'],
            'appraise_info_additional_photo'      => $data['appraise_info']['additional_photo'],
            'appraise_info_time_start'            => $data['appraise_info']['time_start'],
            'appraise_info_time_info'             => $data['appraise_info']['time_info'],
            'appraise_info_time_end'              => $data['appraise_info']['time_end'],
            'appraise_info_status'                => $data['appraise_info']['status'],
            'appraise_info_img_front'             => $data['appraise_info']['img_front'],
            'appraise_info_img_behind'            => $data['appraise_info']['img_behind'],
            'appraise_info_img_right'             => $data['appraise_info']['img_right'],
            'appraise_info_img_left'              => $data['appraise_info']['img_left'],
            'appraise_info_img_dashboard'         => $data['appraise_info']['img_dashboard'],
            'appraise_info_img_passengerseat'     => $data['appraise_info']['img_passengerseat'],
            'appraise_info_img_trunk'             => $data['appraise_info']['img_trunk'],
            'appraise_info_img_machine'           => $data['appraise_info']['img_machine'],
            'appraise_info_img_stnk'              => $data['appraise_info']['img_stnk'],
            'appraise_info_img_bpkb'              => $data['appraise_info']['img_bpkb'],
            'appraise_info_created_at'            => $data['appraise_info']['created_at'],
            'appraise_info_created_by'            => $data['appraise_info']['created_by'],
            'appraise_info_updated_at'            => $data['appraise_info']['updated_at'],
            'appraise_info_updated_by'            => $data['appraise_info']['updated_by'],
            'appraise_info_name'                  => $data['appraise_info']['name'],
            'appraise_info_telephone'             => $data['appraise_info']['telephone'],
            'appraise_info_customer_email'        => $data['appraise_info']['customer_email'],
            'appraise_info_buyer_branch'          => $data['appraise_info']['buyer_branch'],
            'appraise_info_id_sales'              => $data['appraise_info']['id_sales'],
            'appraise_info_id_old_car'            => $data['appraise_info']['id_old_car'],
            'appraise_info_id_new_car'            => $data['appraise_info']['id_new_car'],
            'appraise_info_id_appraise_schedule'  => $data['appraise_info']['id_appraise_schedule'],
            'appraise_info_id_branch'             => $data['appraise_info']['id_branch'],
            'appraise_info_appraisal_scheduled'   => $data['appraise_info']['appraisal_scheduled'],
            'appraise_info_expect_price'          => $data['appraise_info']['expect_price'],
            'appraise_info_down_payment'          => $data['appraise_info']['down_payment'],
            'appraise_info_appraised_price_range' => $data['appraise_info']['appraised_price_range'],
            'appraise_info_max_price_range'       => $data['appraise_info']['max_price_range'],
            'appraise_info_prev_id_tradein'       => $data['appraise_info']['prev_id_tradein'],
            'appraise_info_followup'              => $data['appraise_info']['followup'],
            'appraise_info_no_followup'           => $data['appraise_info']['no_followup'],
            'appraise_info_ip_address'            => $data['appraise_info']['ip_address'],
            'appraise_info_browser_agent'         => $data['appraise_info']['browser_agent'],
            'appraise_info_account_number'        => $data['appraise_info']['account_number'],
            'appraise_info_bank'                  => $data['appraise_info']['bank'],
            'appraise_info_id_user_sales'         => $data['appraise_info']['id_user_sales'],
            'appraise_info_id_prospek'            => $data['appraise_info']['id_prospek'],
            'tradein_car_name'                    => $data['tradein']['car_name'],
            'tradein_car_type'                    => $data['tradein']['car_type'],
            'tradein_tradein_name'                => $data['tradein']['tradein_name'],
            'tradein_ti_status'                   => $data['tradein']['ti_status'],
            'tradein_tradein_phone'               => $data['tradein']['tradein_phone'],
            'tradein_customer_email'              => $data['tradein']['customer_email'],
            'tradein_customer_phone'              => $data['tradein']['customer_phone'],
            'tradein_id_appraise_schedule'        => $data['tradein']['id_appraise_schedule'],
            'tradein_status'                      => $data['tradein']['status'],
            'tradein_appraisal_scheduled'         => $data['tradein']['appraisal_scheduled'],
            'tradein_last_update'                 => $data['tradein']['last_update'],
            'tradein_followup'                    => $data['tradein']['followup'],
            'tradein_no_followup'                 => $data['tradein']['no_followup'],
            'tradein_down_payment'                => $data['tradein']['down_payment'],
            'tradein_branch_address'              => $data['tradein']['branch_address'],
            'tradein_branch_phone'                => $data['tradein']['branch_phone'],
            'tradein_mileage'                     => $data['tradein']['mileage'],
            'tradein_production_year'             => $data['tradein']['production_year'],
            'tradein_vehicle_number'              => $data['tradein']['vehicle_number'],
            'tradein_color'                       => $data['tradein']['color'],
            'tradein_tahun_stnk'                  => $data['tradein']['tahun_stnk'],
            'tradein_color_interior'              => $data['tradein']['color_interior'],
            'tradein_machine_cc'                  => $data['tradein']['machine_cc'],
            'tradein_machine_id'                  => $data['tradein']['machine_id'],
            'tradein_frame_id'                    => $data['tradein']['frame_id'],
            'tradein_car_door'                    => $data['tradein']['car_door'],
            'tradein_car_seat'                    => $data['tradein']['car_seat'],
            'tradein_brand'                       => $data['tradein']['brand'],
            'tradein_id_branch'                   => $data['tradein']['id_branch'],
            'tradein_sales_id'                    => $data['tradein']['sales_id'],
            'tradein_id_branch_appraiser'         => $data['tradein']['id_branch_appraiser'],
            'tradein_max_price'                   => $data['tradein']['max_price'],
            'tradein_id_tradein'                  => $data['tradein']['id_tradein'],
            'tradein_start_time'                  => $data['tradein']['start_time'],
            'tradein_end_time'                    => $data['tradein']['end_time'],
            'tradein_sales_name'                  => $data['tradein']['sales_name'],
            'tradein_sales_phone'                 => $data['tradein']['sales_phone'],
            'tradein_sales_email'                 => $data['tradein']['sales_email'],
            'tradein_appraiser_name'              => $data['tradein']['appraiser_name'],
            'tradein_appraiser_email'             => $data['tradein']['appraiser_email'],
            'tradein_appraiser_phone'             => $data['tradein']['appraiser_phone'],
            'tradein_id_user'                     => $data['tradein']['id_user'],
            'tradein_username'                    => $data['tradein']['username'],
            'tradein_id_appraise_appraiser'       => $data['tradein']['id_appraise_appraiser'],
            'tradein_customer_name'               => $data['tradein']['customer_name'],
            'tradein_appraised_price_range'       => $data['tradein']['appraised_price_range'],
            'tradein_max_price_range'             => $data['tradein']['max_price_range'],
            'tradein_expect_price'                => $data['tradein']['expect_price'],
            'tradein_id_old_car'                  => $data['tradein']['id_old_car'],
            'tradein_appraise_time_start'         => $data['tradein']['appraise_time_start'],
            'tradein_appraise_time_end'           => $data['tradein']['appraise_time_end'],
            'tradein_newcar_name'                 => $data['tradein']['newcar_name'],
            'tradein_newcar_type'                 => $data['tradein']['newcar_type'],
            'tradein_buyer_branch'                => $data['tradein']['buyer_branch'],
            'tradein_offer_start'                 => $data['tradein']['offer_start'],
            'tradein_duration'                    => $data['tradein']['duration'],
            'tradein_id_car_offer'                => $data['tradein']['id_car_offer'],
            'tradein_address'                     => $data['tradein']['address'],
            'tradein_sales_name_2'                => $data['tradein']['sales_name_2'],
            'tradein_sales_email_2'               => $data['tradein']['sales_email_2'],
            'tradein_sales_phone_2'               => $data['tradein']['sales_phone_2'],
            'tradein_id_auto_tradein'             => $data['tradein']['id_auto_tradein'],
        );

        return $arr;
    }


    private function _rename($file, $file_name_template)
    {
        // parsing filename typo
        $file_loc     = explode('pdf', $file);
        $file_loc_fix = $file_loc[0].'.pdf';
        $temp_name    = explode('.', $file_name_template);
        
        // proc rename file 
        $ren          = explode($temp_name[0].'.pdf', $file_loc_fix);
        $_path        = $ren[0];
        

        $rename = rename('/'.$file_loc_fix, '/'.$_path.$this->format_filename); // keep the same folder to just rename 
        if ($rename) {
            return $_path.$this->format_filename;
        }else{
            throw new Exception('Fail rename file template pdf');
        }
    }

    private function _data_json()
    {
        $data = '
            {
                "grade": {
                    "total": 10.8,
                    "score": 3
                },
                "title": "Offering Result",
                "appraise_photos": [
                    {
                        "img_label_name": "stnk",
                        "img_path": "[IMG-REPLACE]https://www.crownplumbing.co/wp-content/uploads/2015/07/placeholder-300x185.gif[/IMG-REPLACE]",
                        "img_description": "18 10 2017 1 tahun\n18 10 2021 5 tahun"
                    },
                    {
                        "img_label_name": "steering_wheel",
                        "img_path": "[IMG-REPLACE]https://erconsult.com.au/wp-content/uploads/2015/04/placeholder-600x400.png[/IMG-REPLACE]",
                        "img_description": ""
                    },
                    {
                        "img_label_name": "odometer",
                        "img_path": "[IMG-REPLACE]https://www.crownplumbing.co/wp-content/uploads/2015/07/placeholder-300x185.gif[/IMG-REPLACE]",
                        "img_description": "123258 km"
                    },
                    {
                        "img_label_name": "interior_row1",
                        "img_path": "[IMG-REPLACE]https://erconsult.com.au/wp-content/uploads/2015/04/placeholder-600x400.png[/IMG-REPLACE]",
                        "img_description": ""
                    },
                    {
                        "img_label_name": "interior_row2",
                        "img_path": "[IMG-REPLACE]https://www.crownplumbing.co/wp-content/uploads/2015/07/placeholder-300x185.gif[/IMG-REPLACE]",
                        "img_description": ""
                    },
                    {
                        "img_label_name": "interior_dashboard",
                        "img_path": "[IMG-REPLACE]https://erconsult.com.au/wp-content/uploads/2015/04/placeholder-600x400.png[/IMG-REPLACE]",
                        "img_description": ""
                    },
                    {
                        "img_label_name": "bagasi",
                        "img_path": "[IMG-REPLACE]https://www.crownplumbing.co/wp-content/uploads/2015/07/placeholder-300x185.gif[/IMG-REPLACE]",
                        "img_description": ""
                    },
                    {
                        "img_label_name": "tampak_samping_kiri",
                        "img_path": "[IMG-REPLACE]https://erconsult.com.au/wp-content/uploads/2015/04/placeholder-600x400.png[/IMG-REPLACE]",
                        "img_description": "Kiri"
                    },
                    {
                        "img_label_name": "tampak_samping_kanan",
                        "img_path": "[IMG-REPLACE]https://www.crownplumbing.co/wp-content/uploads/2015/07/placeholder-300x185.gif[/IMG-REPLACE]",
                        "img_description": "Kanan"
                    },
                    {
                        "img_label_name": "tampak_depan",
                        "img_path": "[IMG-REPLACE]https://erconsult.com.au/wp-content/uploads/2015/04/placeholder-600x400.png[/IMG-REPLACE]",
                        "img_description": "Depan"
                    },
                    {
                        "img_label_name": "tampak_belakang",
                        "img_path": "[IMG-REPLACE]https://www.crownplumbing.co/wp-content/uploads/2015/07/placeholder-300x185.gif[/IMG-REPLACE]",
                        "img_description": "Belakang"
                    },
                    {
                        "img_label_name": "ktp_sesuai_bpkb",
                        "img_path": "[IMG-REPLACE]https://erconsult.com.au/wp-content/uploads/2015/04/placeholder-600x400.png[/IMG-REPLACE]",
                        "img_description": "tidak ada"
                    },
                    {
                        "img_label_name": "ktp_penjual",
                        "img_path": "[IMG-REPLACE]https://www.crownplumbing.co/wp-content/uploads/2015/07/placeholder-300x185.gif[/IMG-REPLACE]",
                        "img_description": "Tidak ada"
                    },
                    {
                        "img_label_name": "kunci_serep",
                        "img_path": "[IMG-REPLACE]https://erconsult.com.au/wp-content/uploads/2015/04/placeholder-600x400.png[/IMG-REPLACE]",
                        "img_description": "tidak ada"
                    },
                    {
                        "img_label_name": "buku_manual",
                        "img_path": "[IMG-REPLACE]https://www.crownplumbing.co/wp-content/uploads/2015/07/placeholder-300x185.gif[/IMG-REPLACE]",
                        "img_description": "ada masih dicari dl"
                    }
                ],
                "appraise_frame": [
                    {
                        "value": "1",
                        "name": "front_side_member_l",
                        "group_name": "Group2",
                        "category": 1,
                        "defect_value": "S",
                        "clamp": 0,
                        "rust": 0,
                        "defect_img": "769--front_side_member_l.jpeg",
                        "defect_img_desc": "Berkarat"
                    }
                ],
                "appraise_engine": [],
                "appraise_interior": [
                    {
                        "name": "Front Door Trim Left",
                        "defect_type": "P",
                        "defect_level": 2,
                        "defect_img": "769--front_door_trim_left.jpeg",
                        "defect_img_desc": "Noda"
                    },
                    {
                        "name": "Front Door Trim Right",
                        "defect_type": "P",
                        "defect_level": 1,
                        "defect_img": "769--front_door_trim_right.jpeg",
                        "defect_img_desc": "Noda"
                    }
                ],
                "appraise_exterior": [
                    {
                        "name": "Front Bumper",
                        "defect_type": "P",
                        "defect_level": 2,
                        "defect_img": "769--front_bumper.jpeg",
                        "defect_img_desc": "Baret dalam",
                        "defect_img2": "769--front_bumper--2.jpeg",
                        "defect_img_desc2": null,
                        "defect_img3": "769--front_bumper--3.jpeg",
                        "defect_img_desc3": null
                    },
                    {
                        "name": "Wheel Front Left",
                        "defect_type": "P",
                        "defect_level": 2,
                        "defect_img": "769--wheel_front_left.jpeg",
                        "defect_img_desc": "Baret dalam",
                        "defect_img2": "",
                        "defect_img_desc2": null,
                        "defect_img3": "",
                        "defect_img_desc3": null
                    },
                    {
                        "name": "Wheel Back Left",
                        "defect_type": "P",
                        "defect_level": 2,
                        "defect_img": "769--wheel_back_left.jpeg",
                        "defect_img_desc": "Baret dalam",
                        "defect_img2": "",
                        "defect_img_desc2": null,
                        "defect_img3": "",
                        "defect_img_desc3": null
                    },
                    {
                        "name": "Rear Bumper",
                        "defect_type": "P",
                        "defect_level": 2,
                        "defect_img": "769--rear_bumper.jpeg",
                        "defect_img_desc": "retak",
                        "defect_img2": "",
                        "defect_img_desc2": null,
                        "defect_img3": "",
                        "defect_img_desc3": null
                    },
                    {
                        "name": "Wheel Back Right",
                        "defect_type": "P",
                        "defect_level": 2,
                        "defect_img": "769--wheel_back_right.jpeg",
                        "defect_img_desc": "Baret dalam",
                        "defect_img2": "",
                        "defect_img_desc2": null,
                        "defect_img3": "",
                        "defect_img_desc3": null
                    },
                    {
                        "name": "Side Mirror Right",
                        "defect_type": "P",
                        "defect_level": 1,
                        "defect_img": "769--side_mirror_right.jpeg",
                        "defect_img_desc": "Baret dalam",
                        "defect_img2": "",
                        "defect_img_desc2": null,
                        "defect_img3": "",
                        "defect_img_desc3": null
                    },
                    {
                        "name": "Wheel Front Right",
                        "defect_type": "P",
                        "defect_level": 2,
                        "defect_img": "769--wheel_front_right.jpeg",
                        "defect_img_desc": "Baret dalam",
                        "defect_img2": "",
                        "defect_img_desc2": null,
                        "defect_img3": "",
                        "defect_img_desc3": null
                    }
                ],
                "appraise_info": {
                    "id_car_appraise": 769,
                    "id_tradein": 1248,
                    "defect_exterior": 7,
                    "defect_interior": 2,
                    "defect_engine": 0,
                    "defect_frame": 1,
                    "additional_photo": "",
                    "time_start": "2017-10-12 10:18:52",
                    "time_info": "2017-10-12 10:31:04",
                    "time_end": "2017-10-12 10:54:59",
                    "status": 4,
                    "img_front": null,
                    "img_behind": null,
                    "img_right": null,
                    "img_left": null,
                    "img_dashboard": null,
                    "img_passengerseat": null,
                    "img_trunk": null,
                    "img_machine": null,
                    "img_stnk": null,
                    "img_bpkb": null,
                    "created_at": "2017-10-11 19:11:59",
                    "created_by": 360,
                    "updated_at": "2017-10-12 14:42:02",
                    "updated_by": 74,
                    "name": "H. Juano",
                    "telephone": "0852123123",
                    "customer_email": "loremipsum@yahoo.co.id",
                    "buyer_branch": "Auto2000 Kapuk",
                    "id_sales": null,
                    "id_old_car": 1248,
                    "id_new_car": 1248,
                    "id_appraise_schedule": 2,
                    "id_branch": 12,
                    "appraisal_scheduled": "2017-10-12",
                    "expect_price": 90000000,
                    "down_payment": null,
                    "appraised_price_range": 70000000,
                    "max_price_range": 80000000,
                    "prev_id_tradein": null,
                    "followup": null,
                    "no_followup": null,
                    "ip_address": "110.136.11.210",
                    "browser_agent": "Mozilla/5.0 (Linux; Android 5.1.1; A37f Build/LMY47V) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.98 Mobile Safari/537.36",
                    "account_number": null,
                    "bank": null,
                    "id_user_sales": 220,
                    "id_prospek": 836
                },
                "tradein": {
                    "car_name": "Minibus",
                    "car_type": "CRV RD5 2 WD 2,0 CKDNT",
                    "tradein_name": "H. Juano",
                    "ti_status": 4,
                    "tradein_phone": "0852123123",
                    "customer_email": "loremipsum@yahoo.co.id",
                    "customer_phone": "0852123123",
                    "id_appraise_schedule": 2,
                    "status": 4,
                    "appraisal_scheduled": "2017-10-12",
                    "last_update": "2017-10-12 14:42:02",
                    "followup": null,
                    "no_followup": null,
                    "down_payment": null,
                    "branch_name": "Auto2000 Mobile Appraiser",
                    "branch_address": "Mobile",
                    "branch_phone": "0852123123",
                    "mileage": 123258,
                    "production_year": 2006,
                    "vehicle_number": "B 123 SI",
                    "color": "Hitam",
                    "tahun_stnk": "2017-10-18",
                    "stnk_5tahun": "2021-08-18",
                    "color_interior": "Cream",
                    "machine_cc": 1998,
                    "machine_id": "K20A5-2600123",
                    "frame_id": "MHRRD475123123123",
                    "car_door": 4,
                    "car_seat": 4,
                    "brand": "Honda",
                    "id_branch": 12,
                    "sales_id": null,
                    "id_branch_appraiser": 48,
                    "max_price": null,
                    "id_tradein": 1248,
                    "start_time": "09:30",
                    "end_time": "11:00",
                    "sales_name": null,
                    "sales_phone": null,
                    "sales_email": null,
                    "appraiser_name": "Yusuf Adi Ariyanto",
                    "appraiser_email": "usur_mipsum@gmail.com",
                    "appraiser_phone": "081380123123",
                    "id_user": 128,
                    "username": "0003",
                    "id_appraise_appraiser": 1397,
                    "customer_name": "H. Juano",
                    "appraised_price_range": 70000000,
                    "max_price_range": 80000000,
                    "expect_price": 90000000,
                    "id_old_car": 1248,
                    "appraise_time_start": "09:30:00",
                    "appraise_time_end": "11:00:00",
                    "newcar_name": "ALL NEW FORTUNER 4x2 2.4 G M/T DSL LUX",
                    "newcar_type": null,
                    "buyer_branch": "Auto2000 Kapuk",
                    "offer_start": "2017-10-12 14:45:00",
                    "duration": "3",
                    "id_car_offer": 518,
                    "address": "Tegal alur cengkareng, ketemuan bareng sales dikantor auto2000 kapuk",
                    "sales_name_2": "ADE CHANDRA",
                    "sales_email_2": "loremipsum@yahoo.co.id",
                    "sales_phone_2": "0852123123",
                    "id_auto_tradein": 559
                }
            }
        ';

        return $data;
    }

    
}
